-include .env
export

include makefiles/help/Makefile
include tool/Makefile

-include makefiles/all-utils/Makefile

include makefiles/services/*/Makefile
include makefiles/repos/*/Makefile
