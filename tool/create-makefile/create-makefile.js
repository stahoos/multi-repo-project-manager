#!/usr/bin/env node

const chalk = require('chalk');
const program = require('commander');
const figlet = require('figlet');
const prompts = require('inquirer');
const path = require('path');
const fs = require('fs');
const recursiveReadSync = require('recursive-readdir-sync');

const reposSubPath = 'repos';
const allUtilsSubPath = 'all-utils';

const message = (text) => {
    console.log('*** ' + text + ' ***');
};

const toTitleCase = (str) => {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};

const getDirPath = (dir) => {
    //dir i.e. repos, all-utils
    return path.join(path.dirname(path.dirname(__dirname)), "makefiles", dir);
};

const loadFile = (dir) => {
    message('Load file');

    let filePath = path.join(__dirname, "_templates", dir, "Makefile");

    return fs.readFileSync(filePath, {encoding: 'utf8'});
};

const saveFile = (dir, name, text) => {
    message('Save file');

    let dirPath = path.join(getDirPath(dir), name.replace(/ /g, '-'));

    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath);
    }

    let filePath = path.join(dirPath, "Makefile");

    const data = new Uint8Array(Buffer.from(text));

    // fs.writeFile(filePath, data, (err) => {
    //     if (err) throw err;
    //     console.log('The file has been saved!');
    // });
    fs.writeFileSync(filePath, data);
};

const replaceText = (data, dictionary) => {
    message('replace file');

    Object.keys(dictionary).forEach(function (element, key, _array) {
        let re = new RegExp("{{" + element + "}}", "gi");
        data = (data.replace(re, dictionary[element]));
    });

    return data;
};

const buildReposDictionary = (answers) => {
    const name = answers.name = answers.name.toLowerCase().replace(' ', '-');
    const label = name.toUpperCase().replace('-', '_');

    return {
        name: name,
        repo_url_label: label,
        repo_url: answers.url.replace("git clone ", ""),
        repo_dir: label + '_REPO_DIR',
        change_dir_cmd: label + '_CHANGE_DIR_CMD',
        fl_name: toTitleCase(name.replace('-', ' ').replace('_', ' ')),
        branch: answers.branch,
        repository_main_branch_label: label + '_BRANCH',
    };
};

const copyFileRepos = (answers, dir) => {
    message('Create Makefile');

    saveFile(dir, answers.name, replaceText(loadFile(dir), buildReposDictionary(answers)));
};

const getTarget = (label) => {
    return label.replace(/-/g, '_') + '_targets';
};

const rebuildAllUtils = () => {
    message('Rebuild all-utils');

    const template = loadFile(allUtilsSubPath);

    const dictionary = ['clone', 'pull', 'build', 'recreate-env', 'switch-to-develop', 'switch-to-master', 'start', 'stop', 'rebuild', 'destroy'];

    const files = recursiveReadSync(getDirPath(reposSubPath)).filter((value) => {
        return value.includes("Makefile");
    });

    let result = {};

    dictionary.forEach((value1) => {
        if (!result[getTarget(value1)]) {
            result[getTarget(value1)] = '';
        }
    });

    files.forEach((value) => {
        let data = fs.readFileSync(value, {encoding: 'utf8'});

        dictionary.forEach((value1) => {
            // console.log(data);

            re = new RegExp('[a-zA-Z-]+' + '-' + value1 + ':', "g");
            let found = data.match(re);

            console.log(found);

            found = found[0].substring(0, found[0].length - 1);

            result[getTarget(value1)] += (found) + ' ';
        })
    });

    saveFile(allUtilsSubPath, '', replaceText(template, result));
};

const createMakefile = () => {
    prompts.prompt([
        {type: 'input', name: 'name', message: 'Provide repo name:'},
        {type: 'input', name: 'url', message: 'Provide ssh command to clone git repo:'},
        {
            type: 'list',
            name: 'branch',
            message: 'Choose main branch:',
            choices: ['master', 'develop'],
            default: 'master'
        }
    ])
        .then(answers => {

            copyFileRepos(answers, reposSubPath);

            rebuildAllUtils();
        });
};

const run = async () => {

    console.log(chalk.red(
        figlet.textSync('Makefile     creator', {horizontalLayout: 'full'}),
        ),
    );

    program
        .version('1.0.0')
        .description('Makefile creator')
        .option('-u, --update-all-utils', 'Update all utils Makefile')
        .option('-c, --create', 'Create repo makefiles, I will ask something')
        .parse(process.argv);

    if (program.updateAllUtils) {
        rebuildAllUtils();
    }

    if (program.create) {
        createMakefile();
    }

    if (!process.argv.slice(2).length) {
        program.outputHelp();
    }
};

run();
